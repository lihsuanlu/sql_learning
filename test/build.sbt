name := "test"

version := "0.1"

scalaVersion := "2.12.5"

lazy val commonDependencies = Seq(
  "mysql" % "mysql-connector-java" % "5.1.6",
  "org.springframework" % "spring-jdbc" % "4.1.6.RELEASE",
  "org.springframework.boot" % "spring-boot-starter-jdbc" % "1.5.2.RELEASE",
  "com.h2database" % "h2" % "1.4.196" % Test,
  "com.zaxxer" % "HikariCP" % "2.7.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final",
  "org.springframework" % "spring-orm" % "4.3.14.RELEASE"
)

libraryDependencies ++= commonDependencies