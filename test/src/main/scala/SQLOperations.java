import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.*;

public class SQLOperations {
    private NamedParameterJdbcTemplate jdbcTemplate;
    private Scanner scan;

    public SQLOperations(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        scan = new Scanner(System.in);
        System.out.println("create new operations object");
    }

    public void insert() {
        System.out.println("What is the brand of the car?");
        String brand = scan.next();
        System.out.println("What is the model of the car?");
        String model = scan.next();
        Map<String, Object> sqlParams = new HashMap<>();
        sqlParams.put("car_id", UUID.randomUUID().toString());
        sqlParams.put("model", model);
        sqlParams.put("brand", brand);
        jdbcTemplate.update("insert into cars (car_id, model, brand) values (:car_id, :model, :brand)", sqlParams);
        System.out.println("Your car has been saved into the database");
    }

    public void update() {
        Map<String, Object> sqlParams = new HashMap<>();
        System.out.println("What is the brand of the car you want to update?");
        sqlParams.put("brand", scan.next());
        System.out.println("What is the model of the car you want to update?");
        sqlParams.put("model", scan.next());
        if(!jdbcTemplate.queryForList("select * from cars where model = :model and brand = :brand", sqlParams).isEmpty()) {
            System.out.println("Your car is present in the database.");
            System.out.println("What is the model you want to update it to?");
            sqlParams.put("updateModel", scan.next());
            jdbcTemplate.update("update cars set model = :updateModel where model=:model and brand = :brand", sqlParams);
            System.out.println("Update is successful");
        } else {
            System.out.println("Your car was not found!");
        }
    }

    public void select() {
        System.out.println("Here are the list of cars currently in the database:");
        jdbcTemplate.query("select * from cars order by brand", rs -> {
            System.out.println("Brand: " + rs.getString("brand") + ", Model: " + rs.getString("model"));
        });
    }

    public void delete() {
        Map<String, Object> sqlParams = new HashMap<>();
        System.out.println("What is the brand of the car you want to remove?");
        sqlParams.put("brand", scan.next());
        System.out.println("What is the model of the car you want to remove?");
        sqlParams.put("model", scan.next());
        if(!jdbcTemplate.queryForList("select * from cars where model = :model and brand = :brand", sqlParams).isEmpty()) {
            System.out.println("Your car is present in the database.");
            jdbcTemplate.update("delete from cars where model=:model and brand = :brand", sqlParams);
            System.out.println("Delete is successful");
        } else {
            System.out.println("Your car was not found!");
        }
    }

}
