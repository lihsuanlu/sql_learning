package HibernateJPA;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class HibernateCars {

    public static void main(String[] args) throws Exception {
        if(args.length == 0) {
            throw new IllegalArgumentException("Please include an operation");
        }
        System.out.println("start operation");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(HibernateContext.class);
        CarService service = ctx.getBean(CarService.class);
        if(args[0].equalsIgnoreCase("insert")) {
            service.insert();
        } else if(args[0].equalsIgnoreCase("update")) {
            service.update();
        } else if(args[0].equalsIgnoreCase("select")) {
            service.select();
        } else if(args[0].equalsIgnoreCase("delete")) {
            service.delete();
        } else {
            throw new IllegalArgumentException("Please insert a valid operation. It can be the following: insert, update, select or delete");
        }
        System.out.println("ended operation");
    }
}
