package HibernateJPA;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

@Repository
@Transactional
public class CarService {

    @PersistenceContext
    private EntityManager em;

    private Scanner scan = new Scanner(System.in);

    public CarService() {
        System.out.println("service created");
    }

    public void insert() {
        CarEntity entity = new CarEntity();
        System.out.println("What is the brand of the car?");
        entity.setBrand(scan.next());
        System.out.println("What is the model of the car?");
        entity.setModel(scan.next());
        em.persist(entity);
        System.out.println("Your car has been saved into the database");
    }

    public void update() {
        Query sql = em.createQuery("from CarEntity where model = :model and brand = :brand");
        System.out.println("What is the brand of the car you want to update?");
        sql.setParameter("brand", scan.next());
        System.out.println("What is the model of the car you want to update?");
        sql.setParameter("model", scan.next());
        List result = sql.getResultList();
        if(!result.isEmpty()) {
            CarEntity entity = (CarEntity)result.get(0);
            System.out.println("Your car is present in the database.");
            System.out.println("What is the model you want to update it to?");
            entity.setModel(scan.next());
            em.merge(entity);
            System.out.println("Update is successful");
        } else {
            System.out.println("Your car was not found!");
        }
    }

    public void select() {
        System.out.println("Here are the list of cars currently in the database:");
        List<CarEntity> resultList = em.createQuery("from CarEntity order by brand").getResultList();
        for(CarEntity entity : resultList) {
            System.out.println("Brand: " + entity.getBrand() + ", Model: " + entity.getModel());
        }
    }

    public void delete() {
        Query sql = em.createQuery("from CarEntity where model = :model and brand = :brand");
        System.out.println("What is the brand of the car you want to delete?");
        sql.setParameter("brand", scan.next());
        System.out.println("What is the model of the car you want to delete?");
        sql.setParameter("model", scan.next());
        List result = sql.getResultList();
        if(!result.isEmpty()) {
            CarEntity entity = (CarEntity)result.get(0);
            em.remove(entity);
            System.out.println("Delete is successful");
        } else {
            System.out.println("Your car was not found!");
        }
    }
}
