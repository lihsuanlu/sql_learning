import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Cars {
    public static void main(String[] args) {
        if(args.length == 0) {
            throw new IllegalArgumentException("Please select an operation to continue");
        }
        System.out.println("Working with the cars table");
        DataSource dataSource = getDataSource();
        SQLOperations operations = new SQLOperations(dataSource);
        if(args[0].equalsIgnoreCase("insert")) {
            operations.insert();
        } else if(args[0].equalsIgnoreCase("update")) {
            operations.update();
        } else if(args[0].equalsIgnoreCase("select")) {
            operations.select();
        } else if(args[0].equalsIgnoreCase("delete")) {
            operations.delete();
        } else {
            throw new IllegalArgumentException("Please insert a valid operation. It can be the following: insert, update, select or delete");
        }

        System.out.println("Ended operation");

    }

    private static DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost/test");
        dataSource.setUsername("root");
        dataSource.setPassword("");
        return dataSource;
    }
}
